package com.adesa.nw.ogg.java;

import lombok.extern.java.Log;
import oracle.goldengate.datasource.*;
import oracle.goldengate.datasource.meta.ColumnMetaData;
import oracle.goldengate.datasource.meta.DsMetaData;
import oracle.goldengate.datasource.meta.TableMetaData;
import oracle.goldengate.datasource.meta.TableName;

import java.util.List;

@Log
public class SrinvHandler extends AbstractHandler {

  private String returnStatus;

  public void setReturnStatus(String returnStatus) {
    this.returnStatus = returnStatus;
  }

  private DsMetaData metaData;

  @Override
  public void init(DsConfiguration conf, DsMetaData metaData) {
    super.init(conf, metaData);
    this.metaData = metaData;
  }

  @Override
  public GGDataSource.Status operationAdded(DsEvent e, DsTransaction tx, DsOperation op) {
    //log.info("**** operationAdded ***");

    //logOperation(op);

    return super.operationAdded(e, tx, op);
  }

  @Override
  public GGDataSource.Status transactionCommit(DsEvent e, DsTransaction tx) {

    log.info("**** transactionCommit ***");

    sleep(1000L);

    for (DsOperation op : tx.getOperations()) {
      logOperation(op);
    }

    log.info("returnStatus = " + returnStatus);
    if (returnStatus != null) {
      GGDataSource.Status status = GGDataSource.Status.valueOf(returnStatus);
      log.info("returning " + status);
      sleep(1000L);
      return status;
    }

    return super.transactionCommit(e, tx);
  }

  @Override
  public GGDataSource.Status metaDataChanged(DsEvent e, DsMetaData meta) {
    return super.metaDataChanged(e, meta);
  }

  @Override
  public void destroy() {
    super.destroy();
  }

  @Override
  public String reportStatus() {
    return "status report...";
  }

  private void logOperation(DsOperation op) {

    StringBuilder sb = new StringBuilder();

    TableName tableName = op.getTableName();
    DsOperation.OpType opType = op.getOperationType();
    List<DsColumn> columns = op.getColumns();
    DsRecord dsRecord = op.getRecord();
    TableMetaData tMeta = this.metaData.getTableMetaData(tableName);

    sb.append("table:").append(tableName.getFullName()).append(", ");
    sb.append("op:").append(opType.name()).append(" -- ");

    int colNum = 0;
    for (DsColumn col : columns) {
      ColumnMetaData cMeta = tMeta.getColumnMetaData( colNum++ );
      sb.append(cMeta.getColumnName()).append(": ").append(col.getBeforeValue()).append("->").append(col.getAfterValue()).append(", ");
    }
    sb.append("\n");

    log.info(sb.toString());
  }

  private GGDataSource.Status getStatus(String name) {
    return GGDataSource.Status.valueOf(name);
  }

  private void sleep(long duration) {
    try {
      log.info("sleeping for " + duration);
      Thread.sleep(duration);
    } catch (InterruptedException e1) {
      e1.printStackTrace();
    }
  }

}
